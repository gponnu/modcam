#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>

using namespace cv;
using namespace std;

int main(int, char**)
{
    Mat frame,eFrame;
    //--- INITIALIZE VIDEOCAPTURE
    VideoCapture cap;
    // open the default camera using default API
    // cap.open(0);
    // OR advance usage: select any API backend
    int deviceID = 0;             // 0 = open default camera
    int apiID = cv::CAP_ANY;      // 0 = autodetect default API
    // open selected camera using selected API
    cap.open(deviceID + apiID);
    // check if we succeeded
    if (!cap.isOpened()) {
        cerr << "ERROR! Unable to open camera\n";
        return -1;
    }
    //--- GRAB AND WRITE LOOP
    cout << "Start grabbing" << endl
        << "Press any key to terminate" << endl;

    int width = 640;
    int height = 480;
    int vidsendsiz = 0;

    int v4l2lo = open("/dev/video2", O_RDWR); 
    if(v4l2lo < 0) {
        std::cout << "Error opening v4l2l device: " << strerror(errno);
        exit(-2);
    }
    {
        struct v4l2_format v;
        int t;
        v.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
        t = ioctl(v4l2lo, VIDIOC_G_FMT, &v);
        if( t < 0 ) {
            exit(t);
        }
        v.fmt.pix.width = width;
        v.fmt.pix.height = height;
        v.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
        vidsendsiz = width * height * 3;
        v.fmt.pix.sizeimage = vidsendsiz;
        t = ioctl(v4l2lo, VIDIOC_S_FMT, &v);
        if( t < 0 ) {
            exit(t);
        }
    }

    
    for (;;)
    {
        // wait for a new frame from camera and store it into 'frame'
        cap.read(frame);
        eFrame = frame;

        // check if we succeeded
        if (frame.empty()) {
            std::cerr << "ERROR! blank frame grabbed\n";
            break;
        }
        unsigned size = eFrame.total() * eFrame.elemSize();
        size_t written = write(v4l2lo, eFrame.data, size);
        if (written < 0) {
            std::cout << "Error writing v4l2l device";
            close(v4l2lo);
            return 1;
        }
        // show live and wait for a key with timeout long enough to show images
        // imshow("Live", eFrame);
        // if (cv::waitKey(5) >= 0)
        //     break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}