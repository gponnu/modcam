README.md

# Introduction

This is an attempt to create a camera input device clone that can segregrate the subject from the background. 

# Installation

## Dependencies

yay -S opencv vtk hdf GLEW CMake

## Make and Install

mkdir build
cd build
cmake ../
make

## Running

sudo modprobe v4l2loopback exclusive_caps=1
./modCam

